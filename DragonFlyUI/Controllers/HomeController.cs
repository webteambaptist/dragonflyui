﻿using DragonFlyUI.Models;
using DragonFlyUI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace DragonFlyUI.Controllers
{
    public class HomeController : Controller
    {
        private readonly LoggerService _logger;
        private readonly FilterService _filterService;

        public HomeController(IConfiguration configuration)
        {
            _logger = CreateLogger();
            _filterService = new FilterService(configuration);
        }
        /// <summary>
        /// Loads the initial page
        /// </summary>
        /// <returns>Only filter options</returns>
        public async Task<IActionResult> Index()
        {
            try
            {
                var ageFilters = await GetAgesRangeFilters();
                var languagesFilters = await GetLanguageFilters();
                var insuranceFilters = await GetInsuranceFilters();
                var allServices = await GetServices();
                var locationFilters = await GetLocationFilters();

                var model = new HomeViewModel
                {
                    Ages = ageFilters,
                    Languages = languagesFilters,
                    Insurances = insuranceFilters,
                    Services = allServices,
                    Locations = locationFilters
                };
                return View(model);
            }
            catch (Exception e)
            {
                _logger.WriteError(
                    "Exception :: Index :: There was an issue returning the filter options " +
                    e.Message);
                TempData.Add("Exception", e.Message);
                return RedirectToAction("Error", "Home");
            }
        }

        /// <summary>
        /// Gets Locations based on the Location Filters
        /// </summary>
        /// <returns>List of Locations</returns>
        public async Task<List<ViewLocation>> GetLocationFilters()
        {
            return await _filterService.GetLocations();
        }
        /// <summary>
        /// Gets a lis to of Insurances
        /// </summary>
        /// <returns>List of Insurances</returns>
        public async Task<List<ViewInsurance>> GetInsuranceFilters()
        {
            return await _filterService.GetInsurancesFilters();
        }
        /// <summary>
        /// Gets a list of Age Ranges 
        /// </summary>
        /// <returns>List of AgeRange</returns>
        public async Task<List<ViewAge>> GetAgesRangeFilters()
        {
            return await _filterService.GetAgeFilters();
        }
        /// <summary>
        /// Gets a list of Specialties
        /// </summary>
        /// <returns>List of Specialties</returns>
        public async Task<List<ViewLanguage>> GetLanguageFilters()
        {
            return await _filterService.GetLanguagesFilters();
        }
        /// <summary>
        /// Gets a list of Specialty Services
        /// </summary>
        /// <returns>List of Specialty Services</returns>
        public async Task<List<ViewService>> GetServices()
        {
            return await _filterService.GetAllServices();
        }
        /// <summary>
        /// This is the Apply Search Filter method that calls microservice and filters data
        /// </summary>
        /// <param name="allLocationsFlag">display all locations</param>
        /// <param name="locations">locations to filter by</param>
        /// <param name="languages">languages to filter by</param>
        /// <param name="insurances">insurances to filter by</param>
        /// <param name="ages">ages to filter by</param>
        /// <param name="services">services to filter by</param>
        /// <returns>filtered data</returns>
        [HttpGet]
        public async Task<IActionResult> ApplySearchFilters(bool allLocationsFlag, bool allServicesFlag, bool allLanguagesFlag, bool allInsurancesFlag, string locations, string languages, string insurances, string ages, string services)
        {
            var jsonData = "";
            try
            {
                var locationList = new List<int>();
                if (allLocationsFlag)
                {
                    var allLocations = await GetLocationFilters();
                    locationList.AddRange(allLocations.Select(loc => loc.Id));
                }
                else
                {
                    locationList = JsonConvert.DeserializeObject<List<int>>(locations);
                }

                var serviceList = new List<int>();
                if (allServicesFlag)
                {
                    var allServices = await GetServices();
                    serviceList.AddRange(allServices.Select(service => service.Id));
                }
                else
                {
                    serviceList = JsonConvert.DeserializeObject<List<int>>(services);
                }

                var languageList = new List<int>();
                if (allLanguagesFlag)
                {
                    var allLanguages = await GetLanguageFilters();
                    languageList.AddRange(allLanguages.Select(language => language.Id));
                }
                else
                {
                    languageList = JsonConvert.DeserializeObject<List<int>>(languages);
                }

                var insuranceList = new List<int>();
                if (allInsurancesFlag)
                {
                    var allInsurances = await GetInsuranceFilters();
                    insuranceList.AddRange(allInsurances.Select(insurance => insurance.Id));
                }
                else
                {
                    insuranceList = JsonConvert.DeserializeObject<List<int>>(insurances);
                }

                var agesList = JsonConvert.DeserializeObject<List<int>>(ages);

                var searchResults = await _filterService.ApplySearchFilters(locationList, languageList, insuranceList, agesList, serviceList);

                if (searchResults == null) return Json(new { data = jsonData });
                var resources = new List<ResourceSearch>();
                foreach (var result in searchResults)
                {
                    var resourceSearch = new ResourceSearch
                    {
                        Name = result.Resource.Name
                    };
                    var locationString = "";
                    foreach (var location in result.LocationList)
                    {
                        if (string.IsNullOrEmpty(locationString))
                        {
                            locationString = location.Location;
                        }
                        else
                        {
                            locationString = locationString + "," + location.Location;
                        }

                    }

                    resourceSearch.Location = locationString;
                    resourceSearch.PhoneNumber = result.Resource.PhoneNumber;
                    resourceSearch.Website = result.Resource.Website;
                    resourceSearch.EntryDt = result.Resource.EntryDt;
                    resourceSearch.EntryUser = result.Resource.EntryUser;
                    resourceSearch.UpdateDt = result.Resource.UpdateDt;
                    resourceSearch.UpdateUser = result.Resource.UpdateUser;
                    resourceSearch.Id = result.Resource.Id;

                    var insuranceString = "";
                    foreach (var insurance in result.InsuranceList)
                    {
                        if (string.IsNullOrEmpty(insuranceString))
                        {
                            insuranceString = insurance.Name;
                        }
                        else
                        {
                            insuranceString = insuranceString + "," + insurance.Name;
                        }
                    }

                    resourceSearch.Insurance = insuranceString;

                    var languageString = "";
                    foreach (var language in result.LanguageList)
                    {
                        if (string.IsNullOrEmpty(languageString))
                        {
                            languageString = language.Language;
                        }
                        else
                        {
                            languageString = languageString + "," + language.Language;
                        }
                    }

                    resourceSearch.Language = languageString;

                    var agesString = "";
                    foreach (var age in result.AgeList)
                    {
                        if (string.IsNullOrEmpty(agesString))
                        {
                            agesString = age.Age;
                        }
                        else
                        {
                            agesString = agesString + "," + age.Age;
                        }
                    }

                    resourceSearch.Age = agesString;

                    var servicesString = "";
                    foreach (var service in result.ServiceList)
                    {
                        if (string.IsNullOrEmpty(servicesString))
                        {
                            servicesString = service.Service;
                        }
                        else
                        {
                            servicesString = servicesString + "," + service.Service;
                        }
                    }

                    resourceSearch.Service = servicesString;
                    resources.Add(resourceSearch);
                }
                var t = resources.ToList().Distinct();
                jsonData = JsonConvert.SerializeObject(resources);

            }
            catch (Exception e)
            {
                jsonData = "";
                _logger.WriteError(
                    "Exception :: ApplySearchFilters :: There was an issue returning the search results based on the filters" +
                    e.Message);
                _logger.WriteError("Exception :: ApplySearchFilters :: Applied Filters: Locations: " + locations + " :: Insurances: " + insurances + " :: Ages: " + ages + " :: Languages: " + languages + " :: Locations: " + locations + " :: Services: " + services);
            }

            var jsonResult = Json(new { data = jsonData });
            return jsonResult;
        }


        /// <summary>
        /// Creates an instance of the Logger
        /// </summary>
        /// <returns>Logger instance</returns>
        public LoggerService CreateLogger()
        {
            var logger = new LoggerService("Home");
            return logger;
        }
        /// <summary>
        /// This is for displaying the Error Page
        /// </summary>
        /// <returns>Error model</returns>
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            try
            {
                TempData.TryGetValue("Exception", out var exc);//TempData["Exception"].ToString();
                var exception = exc.ToString();
                var errorViewModel = new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier,
                    Exception = exception
                };
                return View(errorViewModel);
            }
            catch (Exception e)
            {
                var errorViewModel = new ErrorViewModel
                {
                    RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier
                };
                return View(errorViewModel);
            }
        }
    }
}
