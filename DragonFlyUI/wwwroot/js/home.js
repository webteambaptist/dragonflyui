﻿var resultsTable;
var locationIds = [];
var insuranceIds = [];
var ageIds = [];
var languageIds = [];
var serviceIds = [];
var allLocationsFlag = false;
var allServicesFlag = false;
var allLanguagesFlag = false;
var allInsurancesFlag = false;
var mainUrl;

$(document).ready(function () {
    if (window.location.href.indexOf("dragonflycommunityresources") > -1 || window.location.href.indexOf("DragonFlyCommunityResources") > -1) {
        mainUrl = '/DragonFlyCommunityResources/';
    } else {
        mainUrl = '../';
    }

    $("#selectedLanguagesFilters").hide();
    $("#selectedInsuranceFilters").hide();
    $("#selectedAgeFilters").hide();
    $("#selectedServicesFilters").hide();
    $("#selectedLocationFilters").hide();
    $("#serviceResults").hide();
});


function GetFilterOptions() {
    $("#myModal").modal("show");
}
function HideFilterButtons() {
    $("#searchOptions").hide();
    $("#btnClearAllFilters").hide();
}

function ClearAvailableFilters() {
    //Clear all filter fields
    $("#locationsFromFilter").html('');
    $("#languagesFromFilter").html('');
    $("#insurancesFromFilter").html('');
    $("#agesFromFilter").html('');
    $("#servicesFromFilter").html('');
 
    locationIds = [];
    insuranceIds = [];
    ageIds = [];
    languageIds = [];
    serviceIds = [];
    allLocationsFlag = false;
    allServicesFlag = false;
    allLanguagesFlag = false;
    allInsurancesFlag = false;


    //Clears all of the selected checkboxes from all filters.
    $("input[type=checkbox]").prop("checked", false);

    $("#selectedLocationFilters").hide();
    $("#selectedLanguagesFilters").hide();
    $("#selectedInsuranceFilters").hide();
    $("#selectedAgeFilters").hide();
    $("#selectedServicesFilters").hide();

    $("#serviceResults").hide();
}

function AddFilterOption(filterType, id) {
    var selected = [];
    $("input:checkbox[name=" + filterType + "]:checked").each(function () {
        selected.push($(this).val());
    });
    //if (filterType === "AllLocations") {
    //    $("input:checkbox[name=Locations]:not(:checked)").each(function() {
    //        if ($("#AllLocations").prop('checked') === true) {
    //            allLocationsFlag = true;
    //        } else {
    //            $("#locationsFromFilter").html('');
    //        }
    //    });

    //    $("#selectedLocationFilters").show();
    //    $("#locationsFromFilter")[0].innerText = selected.join(", ");
    //} else {

    if ($("#AllLocations").prop('checked') === true) {
        allLocationsFlag = true;
    } else {
        allLocationsFlag = false;
    }

    if ($("#AllServices").prop('checked') === true) {
        allServicesFlag = true;
    } else {
        allServicesFlag = false;
    }

    if ($("#AllLanguages").prop('checked') === true) {
        allLanguagesFlag = true;
    } else {
        allLanguagesFlag = false;
    }

    if ($("#AllInsurances").prop('checked') === true) {
        allInsurancesFlag = true;
    } else {
        allInsurancesFlag = false;
    }

        switch (filterType) {           
        case "Locations":
            $("#selectedLocationFilters").show();
            $("#locationsFromFilter")[0].innerText = selected.join(",");
            locationIds.push(id);
                break;
            case "Services":
                $("#selectedServicesFilters").show();
                $("#servicesFromFilter")[0].innerText = selected.join(", ");
                serviceIds.push(id);
                break;
            case "Ages":
                $("#selectedAgeFilters").show();
                $("#agesFromFilter")[0].innerText = selected.join(", ");
                ageIds.push(id);
                break;
        case "Languages":
            $("#selectedLanguagesFilters").show();
            $("#languagesFromFilter")[0].innerText = selected.join(", ");
            languageIds.push(id);
            break;
        case "Insurances":
            $("#selectedInsuranceFilters").show();
            $("#insurancesFromFilter")[0].innerText = selected.join(", ");
            insuranceIds.push(id);
            break;       
            case "AllLocations":
                $("input:checkbox[name=Locations]:not(:checked)").each(function () {
                    if (!allLocationsFlag) {
                        $("#locationsFromFilter").html('');
                    }
                });
                $("#selectedLocationFilters").show();
                $("#locationsFromFilter")[0].innerText = selected.join(", ");
                break;
            case "AllServices":
                $("input:checkbox[name=Services]:not(:checked)").each(function () {
                    if (!allServicesFlag) {
                        $("#servicesFromFilter").html('');
                    }
                });
                $("#selectedServicesFilters").show();
                $("#servicesFromFilter")[0].innerText = selected.join(", ");
                break;
            case "AllLanguages":
                $("input:checkbox[name=Languages]:not(:checked)").each(function () {
                    if (!allLanguagesFlag) {
                        $("#languagesFromFilter").html('');
                    }
                });
                $("#selectedLanguagesFilters").show();
                $("#languagesFromFilter")[0].innerText = selected.join(", ");
                break;
            case "AllInsurances":
                $("input:checkbox[name=Insurances]:not(:checked)").each(function () {
                    if (!allInsurancesFlag) {
                        $("#insurancesFromFilter").html('');
                    }
                });
                $("#selectedInsuranceFilters").show();
                $("#insurancesFromFilter")[0].innerText = selected.join(", ");
                break;
        /*}*/
    }
}

function ApplyFilters() {
    var locations = JSON.stringify(locationIds);
    var languages = JSON.stringify(languageIds);//$("#languagesFromFilter")[0].innerText;
    var insurances = JSON.stringify(insuranceIds);//$("#insurancesFromFilter")[0].innerText;
    var ages = JSON.stringify(ageIds);//$("#agesFromFilter")[0].innerText;
    var services = JSON.stringify(serviceIds);//$("#servicesFromFilter")[0].innerText;

    if (locations === "") {
        $("#selectedLocationFilters").hide();
    }
    if (languages === "") {
        $("#selectedLanguagesFilters").hide();
    }

    if (insurances === "") {
        $("#selectedInsuranceFilters").hide();
    }

    if (ages === "") {
        $("#selectedAgeFilters").hide();
    }

    if (services === "") {
        $("#selectedServicesFilters").hide();
    }

    GenerateServiceResultsTable(locations, languages, insurances, ages, services);
}

function GenerateServiceResultsTable(locations, languages, insurances, ages, services) {
    resultsTable = $("#ServicesTable").DataTable({
        "dom": '<"top"l>rt<"bottom"ip><"clear">',
        "destroy": true,
        "bAutoWidth": true,
        "ajax": {
            "url": mainUrl + "Home/ApplySearchFilters",
            "error": function (errResp) {
                alert("There was an issue processing your request. Please try again.");
            },
            "data": {
                "allLocationsFlag": allLocationsFlag,
                "allServicesFlag": allServicesFlag,
                "allLanguagesFlag": allLanguagesFlag,
                "allInsurancesFlag": allInsurancesFlag,
                "locations" : locations,
                "languages": languages,
                "insurances": insurances,
                "ages": ages,
                "services": services
            },
            "dataSrc": function (json) {
                var data = [];
                if (json.data.length > 0) {
                    data = JSON.parse(json.data);
                }
                return data;
            },
            "type": "GET",
            "datatype": "json"
        },
        "language": {
            "emptyTable": "There are currently no Resources to display."
        },
        "columns": [
            {
                "data": "Name",
                sDefaultContent: "",
                "render": function(name, type, data, meta) {

                    if (data.Website === "N/A" || data.Website == null) {
                        return "<h5><i><u>" +
                            data.Name +
                            "</u></i></h5><br/><p><b>Ages: </b><br>" +
                            data.Age +
                            "</p><p><b>N/A</b></p>";
                    } else if (data.Website.includes(';')) {

                        var arr = data.Website.split(';');

                        var results = "<h5><i><u>" +
                            data.Name +
                            "</u></i></h5><br/><p><b>Ages: </b><br>" +
                            data.Age +
                            "</p>";

                        for (var i = 0; i < arr.length; i++) {
                            results +=
                                "<p><a id='appUrl' class='btn btn-info' target='_blank' title='View Website' href='" +
                                arr[i] +
                                "'>View Website</a></p>";
                        }
                        return results;
                    } else {
                        return "<h5><i><u>" +
                            data.Name +
                            "</u></i></h5><br/><p><b>Ages: </b><br>" +
                            data.Age +
                            "</p><p><a id='appUrl' class='btn btn-info' target='_blank' title='View Website' href='" +
                            data.Website +
                            "'>View Website</a></p>";
                    }
                }
            },
            {
                "data": "Location",
                sDefaultContent: "",
                "render": function (name, type, data, meta) {
                    return "<p><b>Location: </b><br>" + data.Location + "</p><p><b>Phone Number: </b><br>" + data.PhoneNumber + "</p>";
                }
            },
            { "data": "Service" },
            { "data": "Language" },
            { "data": "Insurance" }
        ],
        "columnDefs": [
            {
                targets: 0,
                width: "20%",
                className: 'text-center'
            },
            {
                targets: 1,
                width: "20%",
                className: 'text-center'
            },
            {
                targets: 2,
                className: 'text-center'
            },
            {
                targets: 3,
                className: 'text-center'
            },
            {
                targets: 4,
                className: 'text-center'
            }
        ],
        "pageLength": 10,
        "pagingType": "full_numbers",
        responsive: true
    });

    $("#serviceResults").show();
}

$("#searchBox").keyup(function () {
    resultsTable.rows().search(this.value).draw();
});