using System;

namespace DragonFlyUI.Models
{
    public class ErrorViewModel
    {
        public string RequestId { get; set; }
        public string Exception { get; set; }
        public bool ShowRequestId => !string.IsNullOrEmpty(RequestId);
    }
}
