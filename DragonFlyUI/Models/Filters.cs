﻿using System.Collections.Generic;

namespace DragonFlyUI.Models
{
    public class Filters
    {
        public List<int> AgeFilters {get; set; }
        public List<int> InsuranceFilters { get; set; }
        public List<int> LanguageFilters { get; set; }
        public List<int> LocationFilters { get; set; }
        public List<int> ServiceFilters { get; set; }
    }
}
