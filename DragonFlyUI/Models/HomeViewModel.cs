﻿using System.Collections.Generic;

namespace DragonFlyUI.Models
{
    public class HomeViewModel
    {
        public List<ViewLocation> Locations { get; set; }
        public List<ViewInsurance> Insurances { get; set; }
        public List<ViewAge> Ages { get; set; }
        public List<ViewLanguage> Languages { get; set; }
        public List<ViewService> Services { get; set; }
    }
}
