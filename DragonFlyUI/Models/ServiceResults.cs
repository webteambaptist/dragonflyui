﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace DragonFlyUI.Models
{
    public class ServiceResults
    {
        public Resource Resource { get; set; }
        public List<ViewInsurance> InsuranceList { get; set; }
        public List<ViewLanguage> LanguageList { get; set; }
        public List<ViewLocation> LocationList { get; set; }
        public List<ViewService> ServiceList { get; set; }
        public List<ViewAge> AgeList { get; set; }
    }

    public class Resource
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string Website { get; set; }
        public string EntryUser { get; set; }
        public DateTime EntryDt { get; set; }
        public string UpdateUser { get; set; }
        public DateTime? UpdateDt { get; set; }
    }

    public class ViewInsurance
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class ViewLanguage
    {
        public int Id { get; set; }
        public string Language { get; set; }
    }

    public class ViewLocation
    {
        public int Id { get; set; }
        public string Location { get; set; }
        public string ZipCode { get; set; }
    }

    public class ViewService
    {
        public int Id { get; set; }
        public string Service { get; set; }
    }

    public class ViewAge
    {
        public int Id { get; set; }
        public string Age { get; set; }
    }
}
