﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DragonFlyUI.Models
{
    public class FilterOptions
    {
        public string Filter { get; set; }
        public string FilterSelectionName { get; set; }
        public string FilterKey { get; set; }
    }
}
