﻿namespace DragonFlyUI.Models
{
    public class ResourceSearch : Resource
    {
        public string Insurance { get; set; }
        public string Location { get; set; }
        public string Language { get; set; }
        public string Age { get; set; }
        public string Service { get; set; }
    }
}
