﻿using DragonFlyUI.Models;
using DragonFlyUI.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;

namespace DragonFlyUI.Services
{
    public class FilterService : IFilterService
    {
        private readonly IConfiguration _config;

        public FilterService(IConfiguration config)
        {
            _config = config;
        }

        public async Task<List<ViewAge>> GetAgeFilters()
        {
            var serviceUri = $"{_config["AppSettings:RestUrl"]}/{_config["AppSettings:GetAgeFilters"]}";
            using var client = new HttpClient();

            using var response = await client.GetAsync(serviceUri);
            if (response.StatusCode != HttpStatusCode.OK) return null;
            var apiResult = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ViewAge>>(apiResult);
        }

        public async Task<List<ViewLanguage>> GetLanguagesFilters()
        {
            var serviceUri = $"{_config["AppSettings:RestUrl"]}/{_config["AppSettings:GetLanguagesFilters"]}";
            using var client = new HttpClient();

            using var response = await client.GetAsync(serviceUri);
            if (response.StatusCode != HttpStatusCode.OK) return null;
            var apiResult = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ViewLanguage>>(apiResult);
        }

        public async Task<List<ViewInsurance>> GetInsurancesFilters()
        {
            var serviceUri = $"{_config["AppSettings:RestUrl"]}/{_config["AppSettings:GetInsurancesFilters"]}";
            using var client = new HttpClient();

            using var response = await client.GetAsync(serviceUri);
            if (response.StatusCode != HttpStatusCode.OK) return null;
            var apiResult = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ViewInsurance>>(apiResult);
        }

        public async Task<List<ViewService>> GetAllServices()
        {
            var serviceUri = $"{_config["AppSettings:RestUrl"]}/{_config["AppSettings:GetAllServices"]}";
            using var client = new HttpClient();

            using var response = await client.GetAsync(serviceUri);
            if (response.StatusCode != HttpStatusCode.OK) return null;
            var apiResult = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ViewService>>(apiResult);
        }

        public async Task<List<ViewLocation>> GetLocations()
        {
            var serviceUri = $"{_config["AppSettings:RestUrl"]}/{_config["AppSettings:GetLocationFilters"]}";
            using var client = new HttpClient();

            using var response = await client.GetAsync(serviceUri);
            if (response.StatusCode != HttpStatusCode.OK) return null;
            var apiResult = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ViewLocation>>(apiResult);
        }

        public async Task<List<ServiceResults>> ApplySearchFilters(List<int> locations, List<int> languages, List<int> insurances, List<int> ages, List<int> services)
        {
            var filters = new Filters
            {
                AgeFilters = ages,
                InsuranceFilters = insurances,
                LanguageFilters = languages,
                ServiceFilters = services,
                LocationFilters = locations
            };
            using var client = new HttpClient();
            var filterJson = JsonConvert.SerializeObject(filters);
            var restUrl = _config["AppSettings:RestUrl"].ToString();
            var applySearchFilter = _config["AppSettings:ApplySearchFilters"];

            client.DefaultRequestHeaders.Add("filters", filterJson);

            using var response = await client.GetAsync($"{restUrl}/{applySearchFilter}");
            if (response.StatusCode != HttpStatusCode.OK) return null;
            var apiResult = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<ServiceResults>>(apiResult);
        }
    }
}
