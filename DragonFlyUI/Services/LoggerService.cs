﻿using NLog;
using NLog.Config;
using NLog.Targets;
using System;

namespace DragonFlyUI.Services
{
    public class LoggerService
    {
        private static readonly Logger Logger = LogManager.GetCurrentClassLogger();
        public LoggerService(string fileName)
        {
            var config = new LoggingConfiguration();
            var logFile = new FileTarget("logfile")
                {FileName = $"Logs\\{fileName}-{DateTime.Today:MM-dd-yy}.log"};
            config.AddRule(LogLevel.Info, LogLevel.Fatal, logFile);
            LogManager.Configuration = config;
        }

        public void WriteInfo(string text)
        {
            Logger.Info(text);
        }

        public void WriteError(string text)
        {
            Logger.Error(text);
        }
    }
}
