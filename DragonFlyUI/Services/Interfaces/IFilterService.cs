﻿using DragonFlyUI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DragonFlyUI.Services.Interfaces
{
    public interface IFilterService
    {
        Task<List<ViewAge>> GetAgeFilters();
        Task<List<ViewLanguage>> GetLanguagesFilters();
        Task<List<ViewInsurance>> GetInsurancesFilters();
        Task<List<ViewService>> GetAllServices();
        Task<List<ServiceResults>> ApplySearchFilters(List<int> locations, List<int> languages, List<int> insurances, List<int> ages, List<int> services);
    }
}
